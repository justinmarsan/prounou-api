var express = require('express')
var fs = require('fs')
var bodyParser = require('body-parser')
var sha256 = require('js-sha256');

var app = express()

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function (req, res) {
  let data = [];
  fs.readdirSync('data/').forEach(file => {
    const fileContent = JSON.parse(fs.readFileSync('data/' + file, 'utf-8'));
    data.push(fileContent);
  })
  res.send(data);
})

app.post('/', function(req, res) {
  const data = {...req.body};
  data.user = sha256(data.email);
  fs.writeFileSync('data/'+data.user+'.json', JSON.stringify(data));
  res.send(data);
})

app.get('/:id', function (req, res) {
  try {
    const data = fs.readFileSync('data/' + req.params.id + '.json', 'utf-8')
    res.send(data)
  }
  catch {
    console.log('/:id \t No such file \t ' + req.params.id);
    res.send("{}")
  }
})

var server = app.listen('8080', function () {
  console.log('Server running on http://localhost:' + server.address().port)
})
